#!/bin/bash
sudo apt update -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update -y
sudo apt upgrade -y
sudo apt install docker.io -y
sudo curl -L --fail https://github.com/docker/compose/releases/download/1.29.2/run.sh -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

wget https://gitlab.com/wpopera-dxp/integration-hub-bash-and-configurations/-/raw/master/docker-compose.yml\?inline\=false  -O docker-compose.yml
wget https://gitlab.com/wpopera-dxp/integration-hub-bash-and-configurations/-/raw/master/.env?inline=false -O .env
curl https://integrationhub.wpopera.cloud/webhook/b36d121d-c7f9-49f2-a5d3-821ca99a4a61